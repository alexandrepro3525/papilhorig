<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PapilhorigController extends AbstractController
{
    /**
     * @Route("/", name="accueil")
     */
    public function index()
        {
            return $this->render('papilhorig/index.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }

    /**
     * @Route("/pressBook", name="PressBook")
     */
    public function pressBook()
        {
            return $this->render('papilhorig/pressBook.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }

    /**
     * @Route("/produits", name="Produits")
     */
    public function produits()
        {
            return $this->render('papilhorig/produits.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }
    
    /**
     * @Route("/galerie", name="Galerie")
     */
    public function galerie()
        {
            return $this->render('papilhorig/galerie.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }

    /**
     * @Route("/creation", name="Creation")
     */
    public function creation()
        {
            return $this->render('papilhorig/creation.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }
    /**
     * @Route("/contact", name="Contact")
     */
    public function contact()
        {
            return $this->render('papilhorig/contact.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }

    /**
     * @Route("/presentation", name="Presentation")
     */
    public function presentation()
        {
            return $this->render('papilhorig/presentation.html.twig', 
            ['controller_name' => 'PapilhorigController']);
        }
}
